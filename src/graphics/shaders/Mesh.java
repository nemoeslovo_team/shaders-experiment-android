/**
 * Defines a mesh for a 3D Object.
 * Mesh consists of triangular faces with normals
 */

package graphics.shaders;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.*;
import java.util.*;

import android.app.Fragment;
import android.content.Context;
import android.util.Log;
import graphics.shaders.vertex.*;

public class Mesh {
	/*************************
	 * PROPERTIES
	 ************************/
	int meshID; // The id of the stored mesh file (raw resource)

	// Constants
	private static final int FLOAT_SIZE_BYTES = 4;
	private static final int SHORT_SIZE_BYTES = 2;
	// the number of elements for each vertex
	// [coordx, coordy, coordz, normalx, normaly, normalz....]
	private final int VERTEX_ARRAY_SIZE = 8;
	
	// if tex coords exist
	private final int VERTEX_TC_ARRAY_SIZE = 8;

	// Normals
	private float _normals[];
	
	// Texture coordinates
	private float _texCoords[];

	// Indices
	private short _indices[];	
	
	// Buffers - index, vertex, normals and texcoords
	private FloatBuffer _nb;
	private ShortBuffer _ib;
	private FloatBuffer _tcb;

	// Normals
	private float[] _faceNormals;
	private int[]   _surroundingFaces; // # of surrounding faces for each vertex

	// Store the context
	Context activity;


	/***************************
	 * CONSTRUCTOR(S)
	 **************************/
	public Mesh(int meshID, Context activity) {
		this.meshID = meshID;
		this.activity = activity;

		loadFile();
	}

	/**************************
	 * OTHER METHODS
	 *************************/

	/**
	 * Tries to load a file - either a .OBJ or a .OFF
	 * @return 1 if file was loaded properly, 0 if not 
	 */
	private int loadFile() {
		//Log.d("Start-loadFile", "Starting loadFile");
		try {
			// Read the file from the resource
			//Log.d("loadFile", "Trying to buffer read");
			InputStream inputStream = activity.getResources().openRawResource(meshID);

			// setup Bufferedreader
			BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));

			// Try to parse the file
			//Log.d("loadFile", "Trying to buffer read2");
			String str = in.readLine();

			// Make sure it's a .OFF file
			if (str.equals("OFF"))
				loadOFF(in);
			else if (str.equals("OBJ"))
				loadOBJ(in);
			
			// index buffer
			_ib = ByteBuffer.allocateDirect(_indices.length
					* SHORT_SIZE_BYTES).order(ByteOrder.nativeOrder()).asShortBuffer();
			_ib.put(_indices);
			_ib.position(0);

			in.close();
			return 1;
		} catch (Exception e) {
			Log.d(this.getClass().getName(), e.toString());
			return 0;
		}
	}

	/**
	 * Loads the .off file
	 * 
	 * OFF FORMAT:
	 * ------------
	 * Line 1
		OFF
	   Line 2
		vertex_count face_count edge_count
	   One line for each vertex:
		x y z 
		for vertex 0, 1, ..., vertex_count-1
	   One line for each polygonal face:
		n v1 v2 ... vn, 
		the number of vertices, and the vertex indices for each face.
	 * 
	 * 
	 * @return 1 if file was loaded properly, 0 if not 
	 */
	private int loadOFF(BufferedReader in) throws Exception {

			String str = in.readLine();

			StringTokenizer tokenizer = new StringTokenizer(str);
			int verticesCount = Integer.parseInt(tokenizer.nextToken());
			int facesCount    = Integer.parseInt(tokenizer.nextToken());

            FloatVertices vertices = new FloatVertices();
			for (int i = 0; i < verticesCount; i++) {
                StringTokenizer stringTokenizer = new StringTokenizer(in.readLine());
                vertices.add(new FloatVertex(stringTokenizer));
			}

			// read faces and setup the index buffer
			// array size
			int arraySize = facesCount * 3;

			// setup the normals
			_normals = new float[verticesCount * this.VERTEX_ARRAY_SIZE];
			_surroundingFaces = new int[verticesCount]; // # of surrounding faces for each vertex


            ShortVertices shortVertices = new ShortVertices();
			for (int i = 0; i < facesCount; i++) {
				str = in.readLine();

                tokenizer = new StringTokenizer(str);
                // number of vertices for the face - make sure it's 3! [Might add support for 4 later]

                short firstV  = Short.parseShort(tokenizer.nextToken());
                short secondV = Short.parseShort(tokenizer.nextToken());
                short thirdV  = Short.parseShort(tokenizer.nextToken());


                ShortVertex shortVertex = new ShortVertex(firstV, secondV, thirdV);
                shortVertices.add(shortVertex);

				// Calculate the face normal
				setFaceNormal(i, firstV, secondV, thirdV);
			}


			return 1;
			
	}



    /**
	 * Loads an OBJ file
	 * OBJ FORMAT:
	 * ----------
	   list of vertices:
	     v x y z
	   list of tex coords:
	     vt u v
	   list of normals:
	     vn x y z
	   list of faces
	     f pos1/tc1/n1 pos2/tc2/n2 pos3/tc3/n3
	 *
	 * @param in The BufferedReader object
	 * @return true = file properly parsed
	 * @throws Exception
	 */
	private int loadOBJ(BufferedReader in) throws Exception {
		try {
			//Log.d("In OBJ:", "First");
			/* read vertices first */
			String str = in.readLine();
			StringTokenizer t = new StringTokenizer(str);

			String type = t.nextToken();

			// keep reading vertices
			int numVertices = 0;
			ArrayList<Float> vs = new ArrayList<Float>(100); // vertices
			ArrayList<Float> tc = new ArrayList<Float>(100); // texture coords
			ArrayList<Float> ns = new ArrayList<Float>(100); // normals

			while(type.equals("v")) {
				//Log.d("In OBJ:", "V: " + str);

				vs.add(Float.parseFloat(t.nextToken())); 	// x
				vs.add(Float.parseFloat(t.nextToken()));	// y
				vs.add(Float.parseFloat(t.nextToken()));	// z

				// next vertex
				str = in.readLine();
				t = new StringTokenizer(str);

				type = t.nextToken();
				numVertices++;
			}

			// read tex coords
			int numTexCoords = 0;
			if (type.equals("vt")) {
				while(type.equals("vt")) {
					tc.add(Float.parseFloat(t.nextToken())); 	// u
					tc.add(Float.parseFloat(t.nextToken()));	// v

					// next texture coord
					str = in.readLine();
					t = new StringTokenizer(str);

					type = t.nextToken();
					numTexCoords++;
				}
			}

			// read vertex normals
			if (type.equals("vn")) {
				while(type.equals("vn")) {
					ns.add(Float.parseFloat(t.nextToken())); 	// x
					ns.add(Float.parseFloat(t.nextToken()));	// y
					ns.add(Float.parseFloat(t.nextToken()));	// y

					// next texture coord
					str = in.readLine();
					t = new StringTokenizer(str);

					type = t.nextToken();
				}
			}


			// create the vertex buffer
			float[] _v = new float[numVertices * 3];
			// create the normal buffer
			float[] _n = new float[numVertices * 3];
			// texcoord
			_texCoords = new float[numTexCoords * 2];

			// copy over data - INEFFICIENT [SHOULD BE A BETTER WAY]
			for(int i = 0; i < numVertices; i++) {
				_v[i * 3] 	 = vs.get(i * 3);
				_v[i * 3 + 1] = vs.get(i * 3 + 1);
				_v[i * 3 + 2] = vs.get(i * 3 + 2);

				_n[i * 3 ] 	= -ns.get(i * 3);
				_n[i * 3 + 1] = -ns.get(i * 3 + 1);
				_n[i * 3 + 2] = -ns.get(i * 3 + 2);

				// transfer tex coordinates
				if (i < numTexCoords) {
					_texCoords[i * 2] 	  = tc.get(i * 2);
					_texCoords[i * 2 + 1] = tc.get(i * 2 + 1);
				}
			}

			// now read all the faces
			String fFace, sFace, tFace;
			ArrayList<Float> mainBuffer = new ArrayList<Float>(numVertices * 6);
			ArrayList<Short> indicesB = new ArrayList<Short>(numVertices * 3);
			StringTokenizer lt, ft; // the face tokenizer
			int numFaces = 0;
			short index = 0;
			if (type.equals("f")) {
				while (type.equals("f")) {
					// Each line: f v1/vt1/vn1 v2/vt2/vn2
					// Figure out all the vertices
					for (int j = 0; j < 3; j++) {
						fFace = t.nextToken();
						// another tokenizer - based on /
						ft = new StringTokenizer(fFace, "/");
						int vert = Integer.parseInt(ft.nextToken()) - 1;
						int texc = Integer.parseInt(ft.nextToken()) - 1;
						int vertN = Integer.parseInt(ft.nextToken()) - 1;

						// Add to the index buffer
						indicesB.add(index++);

						// Add all the vertex info
						mainBuffer.add(_v[vert * 3]); 	 // x
						mainBuffer.add(_v[vert * 3 + 1]);// y
						mainBuffer.add(_v[vert * 3 + 2]);// z

						// add the normal info
						mainBuffer.add(_n[vertN * 3]); 	  // x
						mainBuffer.add(_n[vertN * 3 + 1]); // y
						mainBuffer.add(_n[vertN * 3 + 2]); // z

						// add the tex coord info
						mainBuffer.add(_texCoords[texc * 2]); 	  // u
						mainBuffer.add(_texCoords[texc * 2 + 1]); // v

					}

					// next face
					str = in.readLine();
					if (str != null) {
						t = new StringTokenizer(str);
						numFaces++;
						type = t.nextToken();
					}
					else
						break;
				}
			}

			mainBuffer.trimToSize();
			//Log.d("COMPLETED MAINBUFFER:", "" + mainBuffer.size());

			vertices = new float[mainBuffer.size()];

			// copy over the mainbuffer to the vertex + normal array
			for(int i = 0; i < mainBuffer.size(); i++)
				vertices[i] = mainBuffer.get(i);

			//Log.d("COMPLETED TRANSFER:", "VERTICES: " + vertices.length);

			// copy over indices buffer
			indicesB.trimToSize();
			_indices = new short[indicesB.size()];
			for(int i = 0; i < indicesB.size(); i++) {
				_indices[i] = indicesB.get(i);
			}

			return 1;

		} catch(Exception e) {
			throw e;
		}
	}

    private void setFacesNormal(FloatVertices vertices, ShortVertices indexes) {
        for (FloatVertex vertex : vertices.getVertices()) {
            setFaceNormal();
            vertex.getVertex()
        }
    }

    private void setFaceNormal(FloatVertex firstVertex, FloatVertex secondVertex, FloatVertex thirdVertex) {
        setFaceNormal();
    }

	/**
	 * Sets the face normal of the i'th face
	 * @param i the index of the face
	 * @param firstV first vertex of the triangle
	 * @param secondV second vertex of the triangle
	 * @param thirdV third vertex of the triangle
	 */
	private void setFaceNormal(int i, int firstV, int secondV, int thirdV) {
		// get coordinates of all the vertices
		float v1[] = {vertices[firstV * VERTEX_ARRAY_SIZE], vertices[firstV * VERTEX_ARRAY_SIZE + 1], vertices[firstV * VERTEX_ARRAY_SIZE + 2]};
		float v2[] = {vertices[secondV * VERTEX_ARRAY_SIZE], vertices[secondV * VERTEX_ARRAY_SIZE + 1], vertices[secondV * VERTEX_ARRAY_SIZE + 2]};
		float v3[] = {vertices[thirdV * VERTEX_ARRAY_SIZE], vertices[thirdV * VERTEX_ARRAY_SIZE + 1], vertices[thirdV * VERTEX_ARRAY_SIZE + 2]};

		// calculate the cross product of v1-v2 and v2-v3
		float v1v2[] = {v1[0]-v2[0], v1[1]-v2[1], v1[2]-v2[2]};
		float v3v2[] = {v3[0]-v2[0], v3[1]-v2[1], v3[2]-v2[2]};

		float cp[] = crossProduct(v1v2, v3v2);

		// try normalizing here
		float sqrt = (float)Math.sqrt(cp[0] * cp[0] +
				cp[1] * cp[1] +
				cp[2] * cp[2]);

		cp[0] /= sqrt;
		cp[1] /= sqrt;
		cp[2] /= sqrt;

		if (cp[0] == -0.0f)
			cp[0] = 0.0f;
		if (cp[1] == -0.0f)
			cp[1] = 0.0f;
		if (cp[2] == -0.0f)
			cp[2] = 0.0f;
		// end normalizing

		// set the normal
		_faceNormals[i * 3]     = cp[0];
		_faceNormals[i * 3 + 1] = cp[1];
		_faceNormals[i * 3 + 2] = cp[2];

		vertices[firstV * this.VERTEX_ARRAY_SIZE + 3] += _faceNormals[i * 3];
		vertices[firstV * this.VERTEX_ARRAY_SIZE + 4] += _faceNormals[i * 3 + 1];
		vertices[firstV * this.VERTEX_ARRAY_SIZE + 5] += _faceNormals[i * 3 + 2];

		vertices[secondV * this.VERTEX_ARRAY_SIZE + 3] += _faceNormals[i * 3];
		vertices[secondV * this.VERTEX_ARRAY_SIZE + 4] += _faceNormals[i * 3 + 1];
		vertices[secondV * this.VERTEX_ARRAY_SIZE + 5] += _faceNormals[i * 3 + 2];

		vertices[thirdV * this.VERTEX_ARRAY_SIZE + 3] += _faceNormals[i * 3];
		vertices[thirdV * this.VERTEX_ARRAY_SIZE + 4] += _faceNormals[i * 3 + 1];
		vertices[thirdV * this.VERTEX_ARRAY_SIZE + 5] += _faceNormals[i * 3 + 2];

		// increment # of faces around the vertex
		_surroundingFaces[firstV]++;
		_surroundingFaces[secondV]++;
		_surroundingFaces[thirdV]++;
	}

	/**
	 * Calculates the cross product of two 3d vectors
	 */
	public float[] crossProduct(float[] v0, float[] v1)
	{
		float crossProduct[] = new float[3];

		crossProduct[0] = v0[1] * v1[2] - v0[2] * v1[1];
		crossProduct[1] = v0[2] * v1[0] - v0[0] * v1[2];
		crossProduct[2] = v0[0] * v1[1] - v0[1] * v1[0];

		return crossProduct;
	}



	/***************************
	 * GET/SET
	 *************************/

	public int getMeshID() {
		return meshID;
	}

	public void setMeshID(int meshID) {
		this.meshID = meshID;
	}

	public short[] get_indices() {
		return _indices;
	}

	public FloatBuffer get_vb() {
		return vertices;
	}
	
	public FloatBuffer get_nb() {
		return this._nb;
	}
	
	public ShortBuffer get_ib() {
		return this._ib;
	}

}
