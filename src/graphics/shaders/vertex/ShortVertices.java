package graphics.shaders.vertex;

import graphics.shaders.util.adt.array.ArrayUtils;

import java.nio.Buffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;

public class ShortVertices extends Vertices<ShortVertex> {

    @Override
    public Buffer getBuffer() {
        ShortBuffer buffer = (ShortBuffer) super.getBuffer();

        List<Short> list = new ArrayList<Short>();
        for (int i = 0; i < vertices.size(); i++) {
            list.addAll(vertices.get(i).getVertex());
        }
        short[] array = ArrayUtils.toShortArray(list);

        buffer.put(array);
        return buffer;
    }
}