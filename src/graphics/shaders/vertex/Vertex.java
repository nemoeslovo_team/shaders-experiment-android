package graphics.shaders.vertex;

import java.nio.Buffer;
import java.util.ArrayList;
import java.util.Collection;

public abstract class Vertex<T extends Number> {

    public static final int VERTEX_SIZE = 6;
    Collection<T> vertex;

    public Vertex(T ... x) {
        vertex = new ArrayList<T>(VERTEX_SIZE);

        for (int i = 0; i < x.length; i++) {
            vertex.add(x[i]);
        }
    }

    public Collection<T> getVertex() {
        return vertex;
    }

    public abstract T getZero();

    public abstract Buffer allocateBuffer(int size);
}