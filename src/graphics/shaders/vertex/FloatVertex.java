package graphics.shaders.vertex;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class FloatVertex extends Vertex<Float> {


    public FloatVertex(Float x, Float y, Float z) {
        super(x, y, z, 0f, 0f, 0f);
    }

    public FloatVertex(StringTokenizer tokenizer) {
        super( Float.parseFloat(tokenizer.nextToken())
             , Float.parseFloat(tokenizer.nextToken())
             , Float.parseFloat(tokenizer.nextToken()));
    }

    @Override
    public Float getZero() {
        return 0f;
    }

    public Float getX() {
//        ArrayList<Vertex<Float>> = getVertex();
//        return getVertex()
        return null;
    }

    @Override
    public Buffer allocateBuffer(int size) {
        int floatSizeInBytes = Float.SIZE / 8;
        int bufferSize = floatSizeInBytes * size;
        return ByteBuffer.allocateDirect(bufferSize).order(ByteOrder.nativeOrder()).asFloatBuffer();
    }

}