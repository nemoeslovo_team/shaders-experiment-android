package graphics.shaders.vertex;

import java.nio.Buffer;
import java.util.List;


public abstract class Vertices<T extends Vertex> {

    public static final short SHORT_ZERRO = 0;
    public static final float FLOAT_ZERRO = 0f;
    List<T> vertices;
    Buffer buffer;

    public void add(Vertex vertex) {
        vertices.addAll(vertex.getVertex());
    }

    public Buffer getBuffer() {
        if(buffer == null && vertices.size() > 0) {
            buffer = vertices.get(0).allocateBuffer(vertices.size());
        }
        return buffer;
    }

    public List<T> getVertices() {
        return vertices;
    }
}